import { describe, expect, test } from '@jest/globals';
import {
  fetchIPNSData,
  convertCIDArrayAvailableMolecules,
  convertCIDArrayRequiredForLogin,
  getRequiredForLogin,
} from '../lib/attestations/cid';
import {
  fallbackIPNSFetch,
  getAttesterDIDDoc,
  checkRequiredForLogin,
} from '../lib/attestations';
import {
  atomArray,
  incompleteAtomArray,
  availableMolecules,
  requiredForLogin,
} from './data.json';
import CONFIG from '../lib/attestations/config';

const IPFS = CONFIG.ATTESTOR_DID_IPFS;
const DID = CONFIG.ATTESTOR_DID;

describe('CID relared function tests', () => {
  test('fetchIPNSData IPFS fetch', async () => {
    const ipfsData = await fetchIPNSData(IPFS);
    expect(ipfsData).toHaveProperty('id');
    expect(ipfsData).toHaveProperty('publicKey[0].publicKeyBase58');
    expect(ipfsData).toHaveProperty('@context');
    expect(ipfsData).toHaveProperty('platform.name');
    expect(ipfsData).toHaveProperty('platform.website');
  });
});

describe('fallbackIPNSFetch tests', () => {
  test('fallbackIPNSFetch DID fetch', async () => {
    const ipfsData = await fallbackIPNSFetch(DID);
    expect(ipfsData).toHaveProperty('data.ipfs');
    expect(ipfsData).toHaveProperty('data.ipns');
  });

  test('fallbackIPNSFetch DID invalid', async () => {
    const ipfsData = await fallbackIPNSFetch('');
    expect(ipfsData).toBeNull();
  });
});

describe('getAttesterDIDDoc tests', () => {
  test('getAttesterDIDDoc DID fetch', async () => {
    const ipfsData = await getAttesterDIDDoc(DID);
    expect(ipfsData).toHaveProperty('id');
    expect(ipfsData).toHaveProperty('publicKey[0].publicKeyBase58');
    expect(ipfsData).toHaveProperty('@context');
    expect(ipfsData).toHaveProperty('platform.name');
    expect(ipfsData).toHaveProperty('platform.website');
  });
});

describe('checkRequiredForLogin tests', () => {
  test('checkRequiredForLogin passing', async () => {
    const requiredPresent = await checkRequiredForLogin(atomArray, DID);
    expect(requiredPresent).toBe(true);
  }, 10000);

  test('checkRequiredForLogin failing', async () => {
    const result = await checkRequiredForLogin(incompleteAtomArray, DID);
    expect(result).toBe(false);
  }, 10000);
});

describe('convertCIDArrayAvailableMolecules test', () => {
  test('checkRequiredForLogin fetching 3 CIDs', async () => {
    const availableMol = await convertCIDArrayAvailableMolecules(
      availableMolecules,
    );
    expect(Object.keys(availableMol).length).toBe(availableMolecules.length);
  });
});

describe('convertCIDArrayRequiredForLogin tests', () => {
  test('convertCIDArrayRequiredForLogin fetching 10 CIDs', async () => {
    const reqMol = await convertCIDArrayRequiredForLogin(requiredForLogin);
    const totalAtoms = reqMol.reduce(
      (previousVal, currentValue) => previousVal + currentValue.atoms.length,
      0,
    );
    expect(totalAtoms).toBe(requiredForLogin.length);
  }, 10000);
});

describe('getRequiredForLogin tests', () => {
  test('getRequiredForLogin => getting empty array', async () => {
    const resp = await getRequiredForLogin({});
    expect(resp.length).toBe(0);
  });

  test('getRequiredForLogin => checking old format', async () => {
    const resp = await getRequiredForLogin({
      requiredForLogin: [
        {
          did: 'did:mdip:omni-xs6n-fy2p-qx5z-4ty',
          atoms: ['first_name', 'last_name', 'drivers_license'],
        },
      ],
    });
    expect(resp[0].atoms.length).toBe(3);
  });

  test('getRequiredForLogin => checking new format', async () => {
    const resp = await getRequiredForLogin({
      requiredForLogin,
    });
    const totalAtoms = resp.reduce(
      (previousVal, currentValue) => previousVal + currentValue.atoms.length,
      0,
    );
    expect(totalAtoms).toBe(requiredForLogin.length);
  });
});
