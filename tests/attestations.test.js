import {
  describe, expect, test, afterAll,
} from '@jest/globals';
import { verifyAttestations } from '../lib/attestations/flow';
import { sampleVP, publicKey, requestorDID } from './data.json';
import { sendAttestation } from '../lib/attestations/offload';
import { IPNSHelper, fetchIPFSDecryptData } from '../lib/ipns/helper';
import { PRIVATE_KEY_HEX } from '../lib/attestations/config';
import { offloadUserData } from '../lib/attestations/onboarding';

const ipnsHelper = new IPNSHelper();

describe('verifyAttestations function tests', () => {
  const cid = sampleVP;
  test('verifyAttestations success', async () => {
    const response = await verifyAttestations(cid, ipnsHelper);
    expect(response.success).toBe(true);
  }, 200000);
});

describe('sendAttestation function tests', () => {
  test('sendAttestation success', async () => {
    const response = await sendAttestation(
      { publicKey, requestorDID },
      { general: true },
      ipnsHelper,
    );
    expect(response.success).toBe(true);
  }, 200000);
});

describe('IPNS helper class function tests', () => {
  test('send doc to IPFS', async () => {
    const response = await ipnsHelper.saveDataToIPFSDoc({ diddoc: 'sample' });
    expect(response).toBeTruthy();
  }, 200000);

  test('reconnect IPFS connection', async () => {
    await ipnsHelper.checkAndReconnect();
    expect(ipnsHelper.connected).toBe(true);
  }, 200000);

  test('Convert decrypt CID data', async () => {
    const data = await fetchIPFSDecryptData(
      sampleVP,
      PRIVATE_KEY_HEX,
      ipnsHelper,
    );
    expect(data).toBeTruthy();
  }, 200000);

  test('offloadUserData test', async () => {
    const offloaded = await offloadUserData(
      requestorDID,
      [{ test: true }],
      publicKey,
      ipnsHelper,
    );
    expect(offloaded).toBeTruthy();
  }, 200000);
});

afterAll(() => {
  ipnsHelper.ctrl.disconnect();
});
