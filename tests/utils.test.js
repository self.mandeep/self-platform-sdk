import { describe, expect, test } from '@jest/globals';
import { credential } from './data.json';
import utils from '../lib/utils';

describe('utils function tests', () => {
  test('extractChains success', async () => {
    const response = await utils.helpers.extractChains(credential);
    expect(response.attestor).toBe('omni');
    expect(response.requestor).toBe('omni');
  }, 200000);
});
