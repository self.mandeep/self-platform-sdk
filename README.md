# SELF Platform SDK

`self` package is useful in developing main functionalities needed to implement flows for secure identity & data sharing. The package is mainly for those who are looking into implementing their custom user flow using SELF echosystem.


# Installation

You can add self package by running following command on root of your nodejs project.

```
npm i @self_id/self-platform-sdk --save
```

# Setup

In order to use this package you will need to register yourself using our onboarding system. You can check more detailed documentation [here](https://gitlab.com/selfid-public/docs/-/blob/main/onboarding.md).


## Backend setup

This package is mainly for backend usage. You will need to setup various environment variables in order to use this package without any issues. For detailed documentation on environment variables setup check [link](https://gitlab.com/selfid-public/docs/-/blob/main/environment.md). You can also visit [link](https://gitlab.com/selfid-public/docs/-/blob/main/self-backend.md) for backend documentations.


## Frontend setup

In order to use `SIGN IN WITH YOUR SELF` button you should check [frontend documentation](https://gitlab.com/selfid-public/docs/-/blob/main/self-frontend.md).


# License

SELF sdk is available under the [MIT](./LICENSE) license. 
