const { IPFS_NODE } = require('../ipns/config');

const HOST = 'self-backend-demo-env.yourself.id';
const BACKEND = `https://${HOST}`;

module.exports = {
  SELF_BACKEND: process.env.SELF_BACKEND || BACKEND,
  attestorName: process.env.ATTESTOR_NAME,
  PUBLIC_KEY: process.env.PUBLIC_KEY,
  PRIVATE_KEY: process.env.PRIVATE_KEY,
  PRIVATE_KEY_HEX: process.env.PRIVATE_KEY_HEX,
  PUBLIC_KEY_HEX: process.env.PUBLIC_KEY_HEX,
  ATTESTOR_DID: process.env.ATTESTOR_DID,
  SELF_MARKETPLACE_TOKEN: process.env.SELF_MARKETPLACE_TOKEN,
  ATTESTOR_DID_IPNS: process.env.ATTESTOR_DID_IPNS,
  ATTESTOR_DID_IPFS: process.env.ATTESTOR_DID_IPFS,
  blockchain: process.env.BLOCKCHAIN || 'omni',
  SELF_MARKETPLACE_URL:
    process.env.SELF_MARKETPLACE_URL
    || 'https://marketplace-onboarding-demo-api.yourself.id',
  IPFS_BACKEND_URL: IPFS_NODE,
};
