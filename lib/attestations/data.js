const concatObject = (mainObj, subject) => {
  const bufferObj = {};
  Object.keys(subject).forEach((objKey) => {
    if (!mainObj[objKey]) {
      bufferObj[objKey] = subject[objKey];
    }
  });
  return Object.assign(mainObj, bufferObj);
};

const assignSubject = (subject, key, value) => {
  if (!subject[key]) {
    return Object.assign(subject, { [key]: value });
  }
  return subject;
};

const getVCSubject = (vc) => {
  if (vc.credentialSubject.general) {
    return vc.credentialSubject.general;
  }
  let subject = {};
  Object.keys(vc.credentialSubject).forEach((objKey) => {
    if (objKey !== 'id' || objKey !== 'schema') {
      subject = assignSubject(subject, objKey, vc.credentialSubject[objKey]);
    }
  });
  return subject;
};

const getMultiSubjectFromId = (creds, id) => {
  let subject = {};
  creds.forEach((item) => {
    if (item.id === id) {
      const currentSubj = getVCSubject(item);
      subject = concatObject(subject, currentSubj);
    }
  });
  return subject;
};

module.exports = {
  getMultiSubjectFromId,
  getVCSubject,
};
