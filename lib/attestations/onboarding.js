const debug = require('debug')('self-sdk:onboarding');
const { getMarketPlaceResponse } = require('./axios');
const atom = require('./atoms');
const { SELF_MARKETPLACE_URL, SELF_MARKETPLACE_TOKEN } = require('./config');

class OnboardingHelper {
  constructor() {
    this.authToken = SELF_MARKETPLACE_TOKEN;
    this.domain = SELF_MARKETPLACE_URL;
    this.lastLogin = null;
    debug(`attestor API token found ${this.authToken}`);
  }

  checkAuthToken() {
    if (!this.authToken) {
      debug('no attestor API token found');
      throw new Error('No attestor API token');
    }
  }

  async offloadAttestations(did, cid) {
    this.checkAuthToken();
    const { authToken } = this;
    const response = await getMarketPlaceResponse(
      '/api/attestor/offloadAttestation',
      { did, cid },
      'POST',
      {
        Authorization: `Bearer ${authToken}`,
      },
    );
    debug(`offloadAttestations ${JSON.stringify(response.data)}`);

    if (response.data.success) {
      return true;
    }
    return false;
  }
}

/**
 * Object defined to call `offloadAttestations` function
 */
const onboardingHelper = new OnboardingHelper();

/**
 * Function to offload claims by calling `onboardingHelper.offloadAttestations`
 * @param {*} did
 * @param {*} cid
 * @returns
 */
const offLoadingCID = async (did, cid) => {
  debug('offLoadingCID', did, cid);
  if (cid) {
    await onboardingHelper.offloadAttestations(did, cid);
    return true;
  }
  return false;
};

/**
 * Function to offload User data object
 * @param {*} did
 * @param {*} data
 * @param {*} publicKey
 * @param {*} ipnsHelper
 * @returns
 */
const offloadUserData = async (did, data, publicKey, ipnsHelper) => {
  const cid = await atom.saveMoleculesToIPNS(data, publicKey, ipnsHelper);
  return offLoadingCID(did, cid);
};

module.exports = {
  OnboardingHelper,
  offLoadingCID,
  offloadUserData,
  helper: onboardingHelper,
};
