const debug = require('debug')('self-sdk:attestations/flow.js');
const { getMultiSubjectFromId } = require('./data');
const { verifySign } = require('../utils');
const CONFIG = require('./config');
const {
  UNEXPECTED_ERROR,
  ERROR_DECRYPTING_DATA,
  ERROR_FETCHING_FROM_IPFS,
  VP_NOT_VERIFIED,
  EXPIRED_VP,
  INVALID_ISSUANCE_DATE,
} = require('./messages');

const claimChecks = async (parsedVP) => {
  try {
    const { proof, ...vc } = parsedVP;
    // check VP signature
    const vpVerified = await verifySign(
      JSON.stringify(vc),
      proof.verificationMethod,
      proof.jws,
      CONFIG.blockchain,
    );
    // check if VC is not expired
    const vcNotExpired = vc.verifiableCredential[0].every(
      (vcObj) => new Date(vcObj.expirationDate) > new Date(),
    );
    // check if  issuance date less than now
    const issuanceDatCheck = vc.verifiableCredential[0].every(
      (vcObj) => new Date(vcObj.issuanceDate) < new Date(),
    );
    // check if all VC has credentialSubject
    const allHaveCredSubject = vc.verifiableCredential[0].every(
      (vcObj) => !!vcObj.credentialSubject,
    );
    debug('[expiredVC]', vcNotExpired);
    debug('[sanityCheck]', issuanceDatCheck);
    let error = null;
    if (!vpVerified) {
      error = VP_NOT_VERIFIED;
    } else if (!vcNotExpired) {
      error = EXPIRED_VP;
    } else if (!issuanceDatCheck) {
      error = INVALID_ISSUANCE_DATE;
    }
    return {
      vpVerified,
      vcNotExpired,
      issuanceDatCheck,
      allHaveCredSubject,
      error,
    };
  } catch (error) {
    debug('claimChecks', error);
    return {
      vpVerified: null,
      vcNotExpired: null,
      issuanceDatCheck: null,
      error,
    };
  }
};

const verifyVP = async (parsedVP) => {
  let error = null;
  try {
    const result = await claimChecks(parsedVP);
    ({ error } = result);
    const {
      vpVerified,
      vcNotExpired,
      issuanceDatCheck,
      allHaveCredSubject,
    } = result;
    const success = vpVerified
    && vcNotExpired
    && issuanceDatCheck
    && allHaveCredSubject
    && !error;

    debug('[claimChecks]', JSON.stringify(result));
    if (success) {
      debug('Claim verified');
      return { success, error };
    }
  } catch (err) {
    error = err;
    debug(error);
  }
  debug('claim not verified');
  return { success: false, error };
};

const getParsedData = (data) => (typeof data !== 'object' ? JSON.parse(data) : data);

const fetchIPFSDecryptData = async (cid, privateKey, ipnsHelper) => {
  let error = null;
  try {
    const encryptedData = await ipnsHelper
      .convertIPFSCidToData(cid)
      .catch((err) => {
        error = ERROR_FETCHING_FROM_IPFS;
        debug('convertIPFSCidToData error >>', err);
        return null;
      });
    debug('encryptedData data typeof >>', typeof encryptedData);
    debug('encryptedData data >>', encryptedData);
    const parsedData = getParsedData(encryptedData);
    const data = await ipnsHelper
      .decryptData(parsedData, privateKey)
      .catch((err) => {
        debug('decryptData error >>', err);
        error = ERROR_DECRYPTING_DATA;
        return null;
      });
    return {
      encryptedData,
      data,
      error,
    };
  } catch (err) {
    debug('[fetchIPFSDecryptData] error >>', err);
    error = err;
    return { error, data: null, encryptedData: null };
  }
};

const decryptAllCids = async (cids, PRIVATE_KEY_HEX, ipnsHelper) => {
  const promises = cids.map(async (cid) => fetchIPFSDecryptData(cid, PRIVATE_KEY_HEX, ipnsHelper));
  return Promise.all(promises);
};

const fetchVCDataArray = (vc) => {
  const creds = vc.verifiableCredential[0];
  debug('vc', JSON.stringify(vc));
  const vcs = Array.isArray(creds) ? creds : [creds];
  let atoms = {};
  if (vcs.length) {
    const uniqueItems = {};
    vcs.forEach((item) => {
      debug(item, 'total items');
      if (!uniqueItems[item.id]) {
        uniqueItems[item.id] = [item];
      } else {
        uniqueItems[item.id].push(item);
      }
      debug(item.id, 'total items ', uniqueItems[item.id].length);
    });
    atoms = Object.keys(uniqueItems).map((objKey) => ({
      issuer: uniqueItems[objKey][0].issuer,
      atoms: getMultiSubjectFromId(uniqueItems[objKey], objKey),
    }));
    debug('atoms', JSON.stringify(atoms));
  }
  return atoms;
};

/**
 * Function to confirm verifiable presentations.
 * It fetches all CIDs, decrypt the content, verifies all attestations by calling `verifyVP`
 * then returns data present in all attestations
 * @param {*} userVP string/array of cid
 * @param {*} ipnsHelper
 * @returns
 */
const verifyAttestations = async (userVP, ipnsHelper) => {
  const allVPCids = Array.isArray(userVP) ? userVP : [userVP];
  debug('allVPCids', allVPCids);
  debug('userVP', typeof userVP, Array.isArray(userVP));
  let error = null;
  const response = {
    success: false,
    data: null,
    error: null,
  };
  const allUserVps = await decryptAllCids(
    allVPCids,
    CONFIG.PRIVATE_KEY_HEX,
    ipnsHelper,
  );

  const vpPromises = allUserVps.map(async (resp) => {
    if (resp.error) {
      error = resp.error;
      debug('decryptAllCids resp.error', resp.error);
    }
    debug('decryptAllCids resp.data', JSON.stringify(resp.data));
    return resp.data || null;
  });
  const vps = await Promise.all(vpPromises);
  // might be due to decryption or IPFS fetch error
  if (error) {
    response.error = error;
    return response;
  }

  const resultPromise = vps.map(async (singleVp) => {
    const { error: currError, success } = await verifyVP(singleVp);
    error = currError;
    return success;
  });
  const vpResults = await Promise.all(resultPromise);
  debug('[vpResults]', JSON.stringify(vpResults));
  const allPass = vpResults.every((result) => result === true);
  debug('[allPass]', allPass);
  debug('total vps', vps.length);
  if (allPass) {
    response.data = fetchVCDataArray(vps[0]);
    response.success = true;
    return response;
  }
  response.success = false;
  if (error) {
    response.error = error;
  } else {
    response.error = UNEXPECTED_ERROR;
  }

  return response;
};

module.exports = {
  verifyAttestations,
};
