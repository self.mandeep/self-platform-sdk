const axios = require('axios');
const debug = require('debug')('self-sdk:fetchCID');
const { fetchResponse } = require('./axios');
const { IPFS_BACKEND_URL, SELF_BACKEND, ATTESTOR_DID } = require('./config');

/**
 * Function to fetch attestor's information from IPNS
 * @param {*} ipnsLink
 * @returns
 */
const fetchIPNSData = async (ipnsLink) => {
  debug('ipnsLink fetchIPNSData', ipnsLink);
  const config = {
    method: 'POST',
    url: `${IPFS_BACKEND_URL}/api/v0/cat?arg=${ipnsLink}`,
  };
  debug('[fetchIPNSData]config :>> ', config);
  return new Promise((resolve, reject) => {
    axios(config)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        debug('fetchIPNSData error ', error);
        reject(error);
      });
  });
};

/**
 * function to fetch attester's IPNS data
 * if not passed ATTESTOR_DID is used as did
 * @param {*} did
 * @returns `Object`
 */
const getAttesterDIDDoc = async (did = ATTESTOR_DID) => {
  if (!did) return null;
  const config = {
    method: 'GET',
    url: `${SELF_BACKEND}/fallbackIPNSFetch?did=${did}`,
    headers: {
      Accept: 'application/json',
    },
  };
  const response = await fetchResponse(config);
  // TODO: after IPNS intregration is done
  // add logic to check if above API has returned
  // DID doc via IPNS
  if (response.data?.ipfs) {
    const ipnsData = await fetchIPNSData(response.data.ipfs);
    return ipnsData;
  }
  return null;
};

/**
 * Function to get initial state (set to false) of all present claims
 * @param {*} requiredForLogin
 * @param {*} state
 * @returns
 */
const getAllAtomInitState = (requiredForLogin) => {
  const newState = {};
  requiredForLogin.map((molecule) => {
    // new implementation
    if (molecule?.attester) {
      const elem = `${molecule.did}::${molecule.name}`;
      newState[elem] = false;
    } else {
      // old implementation
      molecule.atoms.map((element) => {
        const elem = `${molecule.did}::${element}`;
        newState[elem] = false;
        return element;
      });
    }
    return molecule;
  });
  return newState;
};

/**
 * Function to fetch claims of attestor with given did
 * @param {*} atomsArray
 * @param {*} did
 * @returns
 */
const fetchAtomsOfAttestor = (atomsArray, did) => {
  const found = atomsArray.filter((atomItem) => atomItem.issuer.id === did);
  return found.length ? found[0] : { atoms: [] };
};

/**
 * Function to check if atomsArray has claims mentioned in requiredForLogin
 * @param {*} requiredForLogin
 * @param {*} atomsArray
 * @returns
 */
const checkIfRequiredforLoginPresent = (requiredForLogin, atomsArray) => {
  const foundStatus = getAllAtomInitState(requiredForLogin);
  requiredForLogin.forEach((loginObject) => {
    const currentObject = fetchAtomsOfAttestor(atomsArray, loginObject.did);
    const presentAtoms = Object.keys(currentObject.atoms);
    presentAtoms.forEach((atom) => {
      const key = `${loginObject.did}::${atom}`;
      foundStatus[key] = true;
    });
  });
  const foundKeys = Object.keys(foundStatus);
  const someAbsent = foundKeys.some((key) => !foundStatus[key]);
  debug('someAbsent', someAbsent);
  return atomsArray.length && !someAbsent;
};

/**
 * Function to convert CIDs to availableMolecules Object
 * @param {*} cids
 * @param {*} ipnsHelper
 * @returns `Array`
 */
const convertCIDArrayAvailableMolecules = async (cids) => {
  const availableMolecules = {};
  try {
    const promises = cids?.map(async (cid) => {
      const data = await fetchIPNSData(`/ipfs/${cid}`);
      debug('convertCIDArrayAvailableMolecules data', data);
      const parsedAtomDef = typeof data === 'string' ? JSON.parse(data) : data;
      availableMolecules[cid] = { ...parsedAtomDef, cid };
    });
    await Promise.all(promises);
    return availableMolecules;
  } catch (error) {
    debug('convertRequiredCIDArray error', error);
    return [];
  }
};

/**
 * Function to convert CIDs to requiredForLogin array Object
 * @param {*} cids
 * @param {*} ipnsHelper
 * @returns `Array`
 */
const convertCIDArrayRequiredForLogin = async (cids) => {
  const requiredForlogin = {};
  debug('convertCIDArrayRequiredForLogin');
  try {
    const promises = cids?.map(async (cid) => {
      const data = await fetchIPNSData(`/ipfs/${cid}`);
      debug('convertCIDArrayRequiredForLogin data', data);
      const { attester, name } = typeof data === 'string' ? JSON.parse(data) : data;
      if (requiredForlogin[attester]) {
        requiredForlogin[attester].atoms.push(name);
      } else {
        requiredForlogin[attester] = {
          did: attester,
          atoms: [name],
        };
      }
    });
    await Promise.all(promises);
    return Object.values(requiredForlogin);
  } catch (error) {
    debug('convertCIDArrayRequiredForLogin error', error);
    return [];
  }
};

/**
 * function to fetch requiredForLogin
 * @param {*} ipnsData
 * @returns `Array`
 */
const getRequiredForLogin = async (ipnsData) => {
  debug('ipnsData typeof', typeof ipnsData);
  if (!ipnsData?.requiredForLogin) {
    return [];
  }
  debug(
    'typeof ipnsData.requiredForLogin[0]',
    typeof ipnsData.requiredForLogin[0],
  );
  if (typeof ipnsData.requiredForLogin[0] === 'string') {
    const values = await convertCIDArrayRequiredForLogin(
      ipnsData.requiredForLogin,
    );
    return values;
  }
  // should be depricated added just to make code backeward compatible
  return ipnsData.requiredForLogin;
};

/**
 * Function to check if Array of atoms have all claims required for login
 * Decision to pass or fail this check dependent on setting `requiredForLogin`
 * @param {*} atomsArray
 * @returns `boolean`
 */
const checkRequiredForLogin = async (atomsArray, did = ATTESTOR_DID) => {
  const ipnsData = await getAttesterDIDDoc(did);
  const requiredForLogin = await getRequiredForLogin(ipnsData);
  if (requiredForLogin.length > 0) {
    return checkIfRequiredforLoginPresent(requiredForLogin, atomsArray);
  }
  // returning true if there is no requiredForLogin
  return true;
};

module.exports = {
  fetchIPNSData,
  getAttesterDIDDoc,
  checkRequiredForLogin,
  convertCIDArrayAvailableMolecules,
  convertCIDArrayRequiredForLogin,
  getRequiredForLogin,
};
