/**
 * Expose all the modules of SELF Platform SDK
 *
 * @exports selfID
 * @type {Object}
 */
const { signTx } = require('mdip/lib/client/mdip');

const { issueNewClaim } = require('./client/self');
const { verifySign, helpers } = require('./utils');
const {
  sendAttestation,
  verifyAttestations,
  checkRequiredForLogin,
  getAttesterDIDDoc,
} = require('./attestations');
const { IPNSHelper } = require('./ipns/helper');
const { getPreparedPayment } = require('./payments/index');

module.exports = {
  signTx,
  issueNewClaim,
  verifySign,
  helpers,
  sendAttestation,
  verifyAttestations,
  checkRequiredForLogin,
  IPNSHelper,
  getPreparedPayment,
  getAttesterDIDDoc,
};
