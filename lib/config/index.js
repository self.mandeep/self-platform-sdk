module.exports = {
  BTC_BLOCKCHAIN: 'btc',
  ETH_BLOCKCHAIN: 'eth',
  OMNI_BLOCKCHAIN: 'omni',
  PRIVATE_DB_MONGO: 'mongodb',
  TESTNET: 'testnet',

  W3C_DID_SCHEMA: 'https://www.w3.org/ns/did/v1',
  ALLOWED_CHAINS: ['eth', 'omni', 'mongodb'],
  SUPPORTED_LOCALES: ['en'],
  DEFAULT_LOCALE: 'en',
  credentialTypes: [
    'dob',
    'ageOver18',
    'ageOver21',
    'friendsCount',
    'isPlatformXUser',
  ],
};
